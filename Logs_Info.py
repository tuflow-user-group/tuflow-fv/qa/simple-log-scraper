import os
import glob

input_folder = r'./'
output_folder = r'./'

logs_summary = os.path.join(output_folder, 'summary.csv')
header = 'log name, completed, elapsed time (s)\n'
with open(logs_summary, 'w') as summary:
    summary.write(header)

logs_list = os.listdir(input_folder)
# logs_list = glob.glob(input_folder + '\*.log')

count_Y = 0
count_total = 0
time_total = 0

for log_file in logs_list:
    if '.log' in log_file:
        elapsed_time = 'none'
            # with open(log_file) as log:
        with open(os.path.join(input_folder, log_file)) as log:
            log_text = log.read()
            if 'Run Successful.' in log_text:
                finished = 'Y'
                count_Y += 1
                elapsed_time_pointer = log_text.index('Elapsed system_time =')
                elapsed_time = (log_text[elapsed_time_pointer + 21:elapsed_time_pointer + 38]).strip()
            else:
                finished = 'N'
                log.seek(0, 0)
                for line in log:
                    if 'elapsed time =' in line:
                        elapsed_time = (line[68:-3]).strip()
            count_total += 1
            if elapsed_time != 'none':
                time_total += float(elapsed_time)

        with open(logs_summary, 'a') as summary:
            summary.write(f'{log_file},{finished},{elapsed_time}\n')
            # summary.write('{0},{1},{2}\n'.format(log_file, finished, elapsed_time))

with open(logs_summary, 'a') as summary:
    summary.write(f'Total, {count_Y} of {count_total}, {time_total}')